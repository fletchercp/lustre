use std::path::Path;

use cstore::keyspace::*;
use errors::*;

pub fn handle_create_keyspace(name: &str, data_directory: &str) -> Result<Keyspace> {
    if !data_directory_exists(data_directory) {
        bail!(ErrorKind::KeyspaceCreationFailed("Data directory does not exist".to_string()))
    }
    Ok(Keyspace::new(name.to_owned(), ReplicationStrategy::None, data_directory.to_owned()))
}

fn data_directory_exists(data_directory: &str) -> bool {
    Path::new(data_directory).exists()
}