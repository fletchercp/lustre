mod constants;
mod errors;
pub mod handler;

extern crate wiskr; 
extern crate cstore;

#[macro_use]
extern crate error_chain;

use std::io::{self};
use std::io::Write;

pub struct Shell {
    keyspaces: Vec<cstore::keyspace::Keyspace>,
}

impl Shell {
    pub fn new() -> Shell {
        Shell {
            keyspaces: vec![],
        }
    }

    pub fn run(&mut self) {
        put_prompt();
        loop {
            let mut input = String::new();
            match io::stdin().read_line(&mut input) {
                Ok(_n) => {
                    if input.chars().nth(0).unwrap() == ':' {
                        self.handle_shell_command(&input);
                        continue;
                    }

                    match wiskr::parser::parse_query(&input) {
                        Ok(query) => {
                            match query {
                                wiskr::parser::WiskrQuery::CreateKeyspace(ref CreateKeyspaceStatement) => {
                                    let result = handler::handle_create_keyspace(&query.name(), "/tmp");
                                    match result {
                                        Ok(ks) => {
                                            self.keyspaces.push(ks);
                                            println!("Keyspace created!");
                                            put_prompt();
                                            continue;
                                        },
                                        Err(e) => {
                                            println!("Error creating keyspace: {}", e.to_string());
                                            put_prompt();
                                            continue;
                                        }
                                    }
                                },
                                _ => {
                                    println!("invalid query");
                                    put_prompt();
                                    continue;
                                }
                            };
                        },
                        Err(e) => {
                            println!("Invalid query: {}", e);
                            put_prompt();
                            continue;
                        }
                    }
                }
                Err(error) => println!("error: {}", error),
            }
        }
    }

    fn handle_shell_command(&self, input: &str) {
        let parts: Vec<&str> = input.split_whitespace().collect();
        if parts[1] == "keyspaces" {
            self.list_keyspaces();
        } else {
            println!("Invalid shell command!");
            put_prompt();
        }
    }

    fn list_keyspaces(&self) {
        if self.keyspaces.len() == 0 {
            println!("There are no active keyspaces!");
        }

        for keyspace in &self.keyspaces {
            println!("{}", keyspace.name);
        }
        put_prompt();
    }
}

fn put_prompt() {
    print!("{}", constants::prompt);
    io::stdout().flush().unwrap();
}