use std::str;
use std::fmt;
use nom::{multispace, alpha};

pub struct ShowKeyspaceStatement {
    pub keyspace: String
}

impl fmt::Display for ShowKeyspaceStatement {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "SHOW KEYSPACE {} ", self.keyspace)
    }
}

named!(pub show<&[u8], ShowKeyspaceStatement>,
    do_parse!(
        tag!("SHOW KEYSPACE") >>
        multispace >>
        name: alpha >>
        (ShowKeyspaceStatement{
            keyspace: String::from(str::from_utf8(name).unwrap())
        })
    )
);