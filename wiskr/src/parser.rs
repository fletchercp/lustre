use std::fmt;
use nom::{IResult};

use create::{CreateKeyspaceStatement, creation};
use show::*;

pub enum WiskrQuery {
    CreateKeyspace(CreateKeyspaceStatement),
    ShowKeyspace(ShowKeyspaceStatement)
}

pub fn parse_query(input: &str) -> Result<WiskrQuery, &str> {

    let q_bytes = String::from(input.trim()).into_bytes();
    match creation(&q_bytes) {
        IResult::Done(_, o) => {
            return Ok(WiskrQuery::CreateKeyspace(o))
        },
        _ => (),
    };

    match show(&q_bytes) {
        IResult::Done(_, o) => return Ok(WiskrQuery::ShowKeyspace(o)),
        _ => (),
    };

    println!("Failed to parse query: {:?}", input);
    Err("failed to parse query")
}

impl fmt::Display for WiskrQuery {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            WiskrQuery::CreateKeyspace(ref create) => write!(f, "{}", create),
            _ => unimplemented!(),
        }
    }
}

impl WiskrQuery {
    pub fn name(&self) -> &str {
        match *self {
            WiskrQuery::CreateKeyspace(ref create) => {
                &create.keyspace
            },
            WiskrQuery::ShowKeyspace(ref show) => {
                &show.keyspace
            }
            _ => unimplemented!(),
        }
    }
}