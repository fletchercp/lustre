use std::str;
use std::str::FromStr;
use std::fmt;
use nom::{IResult, multispace, alpha};

pub struct CreateKeyspaceStatement {
    pub keyspace: String
}

impl fmt::Display for CreateKeyspaceStatement {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "CREATE KEYSPACE {} ", self.keyspace)
    }
}

named!(pub creation<&[u8], CreateKeyspaceStatement>,
    do_parse!(
        tag!("CREATE KEYSPACE") >>
        multispace >>
        name: alpha >>
        (CreateKeyspaceStatement{
            keyspace: String::from(str::from_utf8(name).unwrap())
        })
    )
);