use serde;

use cell::Cell;

#[derive(Serialize)]
pub struct Row {
    id: Vec<u8>,
    cells: Vec<Cell>
}