use serde_bytes;

use cell::{Cell, CellType};
use constants::*;

#[derive(Serialize, Debug)]
pub struct Table {
    name: String,
    id: u64,
    cells: Vec<Cell>
}

impl Table {
    pub fn new(name: String, id: u64) -> Table {
        Table {
            name: name,
            id: id,
            cells: Vec::with_capacity(DEFAULT_CELLS)
        }
    }

    pub fn add_cell(&mut self, cell: Cell) {
        self.cells.push(cell);
    }
}