use table::Table;

#[derive(Debug)]
pub enum ReplicationStrategy {
    None
}

#[derive(Debug)]
pub struct Keyspace {
    pub name: String,
    replication_strategy: ReplicationStrategy,
    data_directory: String,
    tables: Vec<Table>
}

impl Keyspace {
    pub fn new(name: String, replication_strategy: ReplicationStrategy, data_directory: String) -> Keyspace {
        Keyspace {
            name: name,
            replication_strategy: replication_strategy,
            data_directory: data_directory,
            tables: vec![]
        }
    }
}