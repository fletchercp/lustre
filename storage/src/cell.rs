use serde_bytes;

#[derive(Serialize, Debug)]
pub enum CellType {
    Boolean,
    Integer,
    LString,
    Blob,
    Double,
    Timestamp
}

#[derive(Serialize, Debug)]
pub struct Cell {
    pub ctype: CellType,
    #[serde(with = "serde_bytes")]
    pub key: Vec<u8>,
    #[serde(with = "serde_bytes")]
    pub value: Vec<u8>
}

impl Cell {
    pub fn new(ctype: CellType, key: Vec<u8>, value: Vec<u8>) -> Cell {
        Cell{
            ctype: ctype,
            key: key,
            value: value
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_cell() {
        let c = Cell::new(CellType::Boolean, String::from("active").into_bytes(), vec![false as u8]);
    }
}